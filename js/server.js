/*
Título: Configuración de Servidor Express.

Descripción: Este código JavaScript utiliza Express.js para crear un servidor
web que sirve archivos estáticos ubicados en el directorio 'public'. Se define
un middleware para manejar los archivos estáticos, y la aplicación se inicia
para escuchar las solicitudes entrantes en un puerto específico, ya sea el
proporcionado por el entorno o el puerto 3000.

Fecha: 09 de febrero de 2024 */


const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static('public'));

app.listen(PORT, () => {
    console.log(`El servidor está corriendo en el puerto ${PORT}`);
});
