/*
Título: Funciones JavaScript para Manejo de Mapa y Rutas.

Descripción: Este código JavaScript espera a que el DOM esté completamente 
cargado para ejecutar una serie de funciones relacionadas con la creación 
de un mapa Leaflet, la obtención de la ubicación del usuario, el cálculo de 
rutas y la estimación del tiempo de llegada. Utiliza la geolocalización del 
navegador y Leaflet Routing Machine para calcular rutas entre ubicaciones.

Fecha: 09 de febrero de 2024
*/

// Espera a que el DOM esté completamente cargado para ejecutar el código
document.addEventListener('DOMContentLoaded', function () {
    // Crea un mapa Leaflet centrado en una ubicación específica y establece un nivel de zoom
    var mapa = L.map('map').setView([23.238488155275792, -106.36991484699881], 13);
    var marcadorUsuario; // Variable para almacenar el marcador de la ubicación del usuario
    var velocidadPredeterminada = 30; // Velocidad promedio predeterminada (en km/h)

    // Añade una capa de mosaicos de OpenStreetMap al mapa
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mapa);

    // Obtiene la ubicación actual del usuario si la geolocalización está disponible en el navegador
    if ("geolocation" in navigator) {
        // Observa cambios en la posición del usuario y actualiza el mapa en consecuencia
        navigator.geolocation.watchPosition(function(posicion) {
            var latitud = posicion.coords.latitude;
            var longitud = posicion.coords.longitude;
            
            // Crea un icono personalizado para la ubicación actual del usuario
            var iconoUbicacionActual = L.divIcon({
                className: 'current-location-icon',
                html: '<div class="circle"></div>'
            });
            // Crea un marcador en la ubicación actual del usuario y lo agrega al mapa
            marcadorUsuario = L.marker([latitud, longitud], { icon: iconoUbicacionActual }).addTo(mapa);
            mapa.setView([latitud, longitud], 13);
            
            // Muestra las coordenadas actuales del usuario en un label
            document.getElementById("lblUbicacion").textContent = "Ubicación Actual: " + latitud.toFixed(6) + ", " + longitud.toFixed(6);
            
            // Calcula y muestra la hora estimada de llegada al destino si hay una ruta establecida
            if (rutaActual) {
                var destino = rutaActual.getWaypoints()[1].latLng;
                var distanciaAlDestino = marcadorUsuario.getLatLng().distanceTo(destino) / 1000; // Distancia en kilómetros
                
                var tiempoEstimadoVelocidadPredeterminada = calcularTiempoEstimadoLlegada(velocidadPredeterminada, distanciaAlDestino);
                
                document.getElementById("lblTiempoLlegada").textContent = "Tiempo Estimado de Llegada (60 km/h): " + tiempoEstimadoVelocidadPredeterminada;
            }
        }, function(error) {
            console.error('Error al obtener la ubicación: ', error);
        });
    } else {
        console.error('Geolocalización no es compatible en este navegador.');
    }

    // Configura marcadores de destino
    var destinos = [
        { nombre: 'Consultorio 1', coordenadas: [23.238488155275792, -106.36991484699881] },
        { nombre: 'Consultorio 2', coordenadas: [23.21201372173557, -106.42122126554736] },
        { nombre: 'Consultorio 3', coordenadas: [23.268700114647356, -106.42774800324672] },
        { nombre: 'Consultorio 4', coordenadas: [23.233651369773085, -106.42388628669367] },
        { nombre: 'Consultorio 5', coordenadas: [23.254774085423072, -106.41910620652875] },
        { nombre: 'Consultorio 6', coordenadas: [23.239831228757144, -106.43034573165615] },
        { nombre: 'Consultorio 7', coordenadas: [23.24602953624312, -106.38768654699858] }
    ];

    var iconoPersonalizado = L.icon({
        iconUrl: '/img/ubicacion.png',
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -32]
    });

    // Agrega marcadores de destino al mapa y establece eventos de clic para calcular rutas
    destinos.forEach(destino => {
        L.marker(destino.coordenadas, { icon: iconoPersonalizado }).addTo(mapa).bindPopup(destino.nombre).on('click', function(e) {
            calcularRuta(e.latlng);
        });
    });

    // Variable para almacenar la referencia a la ruta actual
    var rutaActual;

    // Función para calcular la ruta utilizando Leaflet Routing Machine
    function calcularRuta(destino) {
        if (marcadorUsuario) {
            var ubicacionUsuario = marcadorUsuario.getLatLng();
            var inicio = ubicacionUsuario;
            var fin = destino;

            // Calcula la distancia al destino
            var distanciaAlDestino = ubicacionUsuario.distanceTo(destino) / 1000; // Distancia en kilómetros

            // Elimina la ruta anterior si existe
            if (rutaActual) {
                mapa.removeControl(rutaActual);
                rutaActual = null; // Indica que no hay ruta actual
            }

            // Calcula la nueva ruta y la agrega al mapa
            rutaActual = L.Routing.control({
                waypoints: [
                    L.latLng(inicio.lat, inicio.lng),
                    L.latLng(fin.lat, fin.lng)
                ],
                routeWhileDragging: true,
                createMarker: function (i, waypoint, n) {
                    // No crea marcadores
                    return null;
                }
            }).addTo(mapa);

            // Calcula y muestra la hora estimada de llegada al destino
            var tiempoEstimadoVelocidadPredeterminada = calcularTiempoEstimadoLlegada(velocidadPredeterminada, distanciaAlDestino);
            calcularTiempoEstimadoLlegadaYMensaje(velocidadPredeterminada, distanciaAlDestino);

            document.getElementById("lblTiempoLlegada").textContent = "Tiempo Estimado de Llegada (60 km/h): " + tiempoEstimadoVelocidadPredeterminada;

        } else {
            alert("No se puede calcular la ruta porque no se ha podido obtener la ubicación del usuario.");
        }
    }

    // Función para calcular la hora estimada de llegada
    function calcularTiempoEstimadoLlegada(velocidad, distancia) {
        var tiempoViajeMinutos = distancia / velocidad * 60; // Tiempo en minutos

        tiempoViajeMinutos += 11; // Suma 11 minutos al tiempo estimado de viaje

        var tiempoViajeMinutosRedondeado = Math.round(tiempoViajeMinutos); // Redondea el tiempo a minutos enteros

        var horas = Math.floor(tiempoViajeMinutosRedondeado / 60);
        var minutos = tiempoViajeMinutosRedondeado % 60;

        var tiempoEstimado = "";

        if (horas > 0) {
            tiempoEstimado += horas + " hora" + (horas > 1 ? "s" : "");
            if (minutos > 0) {
                tiempoEstimado += " y ";
            }
        }

        if (minutos > 0) {
            tiempoEstimado += minutos + " minuto" + (minutos > 1 ? "s" : "");
        }

        return tiempoEstimado;
    }

    // Función para calcular el tiempo estimado de llegada y mostrar un mensaje en los labels
    function calcularTiempoEstimadoLlegadaYMensaje(velocidad, distancia) {
        var tiempoViajeMinutos = distancia / velocidad * 60;

        tiempoViajeMinutos += 10;

        var horaCita = new Date();
        horaCita.setHours(14); // Hora de la cita (14:00)
        horaCita.setMinutes(30); // Minuto de la cita (30)

        var horaSalida = new Date(horaCita.getTime() - (tiempoViajeMinutos * 60000)); // Resta el tiempo estimado de viaje en milisegundos

        var horaSalidaFormateada = ("0" + horaSalida.getHours()).slice(-2) + ":" + ("0" + horaSalida.getMinutes()).slice(-2);

        document.getElementById("lblTiempoLlegada").textContent = "Tiempo Estimado de Llegada (60 km/h): " + horaSalidaFormateada;
        document.getElementById("departure-message").textContent = "Para llegar a tiempo a tu cita de las 14:30 tienes que salir de tu casa a las " + horaSalidaFormateada+".";
    }
});
